import React from 'react';
import { Input, Select } from 'antd';

const operators = ['=', '!=', '>', '>=', '<', '<=', '~'];

export default class FilterInputs extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedValues: new Map(),
    }
  }

  render() {
    return (

      <Input.Group compact>
        <Select
          showSearch
          key={`Select-${this.props.elementKey}`}
          style={{ width: '40%' }}
          placeholder="Filter"
          optionFilterProp="children"
          onChange={(name) => this.props.handleChange({ name }, this.props.elementKey)}
          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          getPopupContainer={trigger => trigger.parentNode}
        >
          {
            (this.props.values || []).map((value) => <Select.Option key={value} value={value}>{value}</Select.Option>)
          }
        </Select>

        <Select
          showSearch
          key={`Select-Op-${this.props.elementKey}`}
          style={{ width: '20%' }}
          optionFilterProp="children"
          defaultValue={operators[0]}
          onChange={(op) => this.props.handleChange({ op }, this.props.elementKey)}
          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          getPopupContainer={trigger => trigger.parentNode}
        >
          {
            operators.map((op) => <Select.Option key={op} value={op}>{op}</Select.Option>)
          }
        </Select>

        <Input
          style={{ width: '40%' }}
          placeholder="Filter value"
          onChange={(e) => this.props.handleChange({ value: e.target.value }, this.props.elementKey)} />

      </Input.Group>

    );
  }
}
