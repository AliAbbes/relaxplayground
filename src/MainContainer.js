import React from 'react';
import { Button, Input, Icon, Select, Spin, notification, ConfigProvider } from 'antd';
import DataInputGroup from './DataInputGroup';
import FilterInputs from './FilterInputs';
import LevelInputs from './LevelInputs';
import ResultPanel from './ResultPanel';
import { generateRequestBody } from './queryGeneration';
import logo from './logo.png'

const defaultMeasure = {
  measureId: 0,
  view: '',
  filters: new Map(),
  levels: new Map(),
}

const defaultFilter = { op: '=' };

const loadingIcon = <Icon type="loading" className="spinning_icon" spin />;

const customizeRenderEmpty = () => (
  <div style={{ textAlign: 'center', marginTop: '1rem' }}>
    <Icon type="stop" style={{ fontSize: '1.5rem' }} />
    <p>No Data</p>
  </div>
);

export default class MainContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      measures: [defaultMeasure],
      dataEndPointUrl: 'http://localhost:8080/globalrelax',
      data: {},
      loading: false,
      error: null,
      reqBody: generateRequestBody([defaultMeasure]),
    };
  }

  handleViewChange(selectedView, selectedmeasureId) {
    this.setState(
      (state) => {
        const measureIndex = state.measures.findIndex(({ measureId }) => measureId === selectedmeasureId);
        const measures = [...state.measures];
        const measure = { ...measures[measureIndex], view: selectedView };
        measures[measureIndex] = measure;
        return ({ measures, reqBody: generateRequestBody(measures) })
      }
    );
  }

  handleLevelChange(level, key, selectedMeasureId) {
    this.setState(
      (state) => {
        const selectedLevel = state.measures.find(({ measureId }) => measureId === selectedMeasureId).levels.get(key);
        const mergedLevel = { ...selectedLevel, ...level };
        const measures = [...state.measures];
        const measureIndex = measures.findIndex(({ measureId }) => measureId === selectedMeasureId);
        const measure = { ...measures[measureIndex], levels: new Map(measures[measureIndex].levels).set(key, mergedLevel) }
        measures[measureIndex] = measure;

        return ({ measures, reqBody: generateRequestBody(measures) });
      }
    );
  }

  handleLevelRemove(key, selectedMeasureId) {
    this.setState(
      (state) => {

        const measureIndex = state.measures.findIndex(({ measureId }) => measureId === selectedMeasureId);
        const selectedLevels = new Map(state.measures[measureIndex].levels);
        selectedLevels.delete(key);

        const measure = { ...state.measures[measureIndex], levels: selectedLevels }
        const measures = [...state.measures]
        measures[measureIndex] = measure;
        return ({ measures, reqBody: generateRequestBody(measures) });
      }
    )
  };

  handleFilterChange(filter, key, selectedMeasureId) {
    this.setState(
      (state) => {
        const selectedFilter = state.measures.find(({ measureId }) => measureId === selectedMeasureId).filters.get(key);
        const mergedFilter = { ...defaultFilter, ...selectedFilter, ...filter };
        const measures = [...state.measures];
        const measureIndex = measures.findIndex(({ measureId }) => measureId === selectedMeasureId);
        const measure = { ...measures[measureIndex], filters: new Map(measures[measureIndex].filters).set(key, mergedFilter) }
        measures[measureIndex] = measure;

        return ({ measures, reqBody: generateRequestBody(measures) });
      }
    );
  }

  handleFilterRemove(key, selectedMeasureId) {
    this.setState(
      (state) => {

        const measureIndex = state.measures.findIndex(({ measureId }) => measureId === selectedMeasureId);
        const selectedFilters = new Map(state.measures[measureIndex].filters);
        selectedFilters.delete(key);

        const measure = { ...state.measures[measureIndex], filters: selectedFilters }
        const measures = [...state.measures]
        measures[measureIndex] = measure;
        return ({ measures, reqBody: generateRequestBody(measures) });
      }
    )
  }

  handleAddMeasure() {
    this.setState(state => {
      const newKey = state.measures.length
        ? state.measures[state.measures.length - 1].measureId + 1
        : 0;
      const measures = [...state.measures, { ...defaultMeasure, measureId: newKey }];
      return ({ measures, reqBody: generateRequestBody(measures) });
    })
  }

  handleRemoveMeasure(keyToRemove) {
    this.setState(state => {
      const measures = state.measures.filter(({ measureId }) => measureId !== keyToRemove);
      return ({ measures, reqBody: generateRequestBody(measures) });
    })
  }

  getDimensions() {
    return this.props.metaModel.level.map(({ name }) => name);
  }

  getLevels(key, selectedMeasureId) {
    const selectedDimensionMetaModel = this.props.metaModel.level.find(({ name }) => {
      const levels = this.state.measures.find(({ measureId }) => measureId === selectedMeasureId).levels;
      const selectedLevel = levels.get(key) || {};
      return name === selectedLevel.dimension
    }) || {}

    return (selectedDimensionMetaModel.level || []).map(({ name }) => name)
  }

  fetchData() {
    const headers = new Headers()
    headers.append('Content-Type', 'application/json');

    this.setState({ loading: true });

    fetch(this.state.dataEndPointUrl, { headers, method: 'POST', credentials: 'include', body: this.state.reqBody })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error(`${response.status}: ${response.statusText}`)
      })
      .then((data) => {
        this.setState({ data, loading: false });
      })
      .catch((error) => {
        this.setState({ loading: false });
        notification.error({
          message: error.message,
          description: error.stack,
        })
      })
  }

  render() {

    return (
      <Spin indicator={loadingIcon} style={{ width: '100%' }} spinning={this.state.loading}>
        
        <div className="app-container">
 
          <div className="header">
     
            <span className="App-Title">Relax Playground </span>

            
            <Button icon="file-search" type="default" onClick={() => this.props.showMetaModel()}> Relax Model </Button>
            <div>

<Button   href="http://localhost:3000 "  variant="outlined" color="secondary">
                  Relax Editor
     
      </Button> 
      
      
      </div> 
          </div>

          <div className="query-bar">
            <Button
              type="primary"
              onClick={() => this.fetchData()}
              style={{ marginRight: '1rem' }}
            >
              Send <Icon type="enter" />
            </Button>
            <Input className="input-query" value={this.state.dataEndPointUrl} onChange={(e) => this.setState({ dataEndPointUrl: e.target.value })} />
            <Button type="primary" onClick={() => this.setState({ modalOpen: true })} style={{ marginLeft: '1rem' }}> Copy as CURL</Button>
          </div>
          <div className="inputs-panel">
            <h3>Views</h3>
            {
              this.state.measures.map(({ measureId }) => (
                <div className="input_group" key={`input-group-${measureId}`}>
                  <ConfigProvider renderEmpty={customizeRenderEmpty}>
                    <div style={{ marginBottom: '1rem' }}>
                      <Select
                        showSearch
                        style={{ width: '100%' }}
                        placeholder="View"
                        optionFilterProp="children"
                        onChange={(value) => this.handleViewChange(value, measureId)}
                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        getPopupContainer={trigger => trigger.parentNode}
                      >
                        {
                          (this.props.metaModel.view.map(view => view.name)).map((value) => <Select.Option key={value} value={value}>{value}</Select.Option>)
                        }
                      </Select>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                      <DataInputGroup
                        handleRemove={(key) => this.handleLevelRemove(key, measureId)}
                      >

                        <LevelInputs
                          handleChange={(level, key) => this.handleLevelChange(level, key, measureId)}
                          dimensions={this.getDimensions()}
                          levels={(key) => this.getLevels(key, measureId)}
                        />
                      </DataInputGroup>

                      <DataInputGroup
                        name="Filters"
                        values={this.props.metaModel.filter.map(filter => filter.name)}
                        handleRemove={(key) => this.handleFilterRemove(key, measureId)}
                      >
                        <FilterInputs
                          handleChange={(filter, key) => this.handleFilterChange(filter, key, measureId)}
                          values={this.props.metaModel.filter.map(filter => filter.name)}
                        />
                      </DataInputGroup>
                    </div>
                    <Icon type="minus-circle-o" className="remove_group_btn" onClick={() => this.handleRemoveMeasure(measureId)} />
                  </ConfigProvider>
                </div>
              ))
            }
            <Button icon="plus" type="dashed" onClick={() => this.handleAddMeasure()} style={{ width: '100%', marginBottom: '1.5rem' }}> More </Button>

            <h3>Request Body</h3>
            <Input.TextArea
              autosize={{ minRows: 2, maxRows: 6 }}
              value={this.state.reqBody}
              onChange={(e) => this.setState({ reqBody: e.target.value })}
              onPressEnter={() => this.fetchData()}
            />
          </div>
          <ResultPanel
            metaModel={this.props.metaModel}
            views={this.state.measures.filter(({ view }) => view !== '').map(({ view }) => view)}
            data={this.state.data}
          />
        </div>
        
      </Spin>
    );
  }
}