import React from 'react';
import { Modal, Tabs } from 'antd';
import ReactJson from 'react-json-view';

const getViewConfig = (metamodel, view) => metamodel.view.find(({ name }) => name === view)

const ResultTableModal = (props) => (
  <Modal width="80%" centered={true} visible={props.visible} onCancel={() => props.closeModal()} footer={null} >
    <Tabs type="card">
      {props.views.map(
        (view, index) => (
          <Tabs.TabPane tab={view} key={index}>
            <ReactJson
              theme="flat"
              iconStyle="triangle"
              style={{ width: '100%' }}
              collapsed={2}
              src={getViewConfig(props.metaModel, view)}
            />
          </Tabs.TabPane>
        )
      )}
    </Tabs>
  </Modal>
)

export default ResultTableModal;