# Relax Playground

A handy tool to write relax queries and display responses through: well formed requests generation, Row Response tree view and and in-grid results

## Getting started

```
npm install
npm start

```
